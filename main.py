import logging
from func import twitter
from func.config import CONFIG

# Log to both stdout and file
logging.basicConfig(filename=CONFIG["LOG_PATH"], level=logging.INFO)
logging.getLogger().addHandler(logging.StreamHandler())

logging.info("RUNNING ENVIRONMENT: {env}".format(env=CONFIG["ENV"]))

twitter.initPostedData()

twitter.authenticate(twitter.startPostLoop)